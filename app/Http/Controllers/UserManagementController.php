<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserManagement;

class UserManagementController extends Controller
{
    /**
     * Create a new user
     *
     * @param  request  $Request
     * @return  request  redirects to previous page
     */
    protected function addUser(request $Request)
    {
        if ($name = $Request->input("name"))
            UserManagement::addUser($name);
        return back();
    }

    /**
     * User management dashboard
     *
     * @param  request  $Request
     * @return  request  redirects to previous page
     */
    protected function dashboard()
    {
        $users = UserManagement::getAllUsers();
        $groups = UserManagement::getAllGroups();
        return view("usermanagement.dashboard", compact("users", "groups"));
    }

    /**
     * Remove user
     *
     * @param  request  $Request
     * @return  request
     */
    protected function removeUser($userId)
    {
        UserManagement::removeUser($userId);
        return redirect()->route("ums");
    }

    /**
     * Create group
     *
     * @param  request
     * @return  request
     */
    protected function addGroup(request $Request)
    {
        $name = $Request->input("name");
        if ($name)
            UserManagement::addGroup($name);
        return back();
    }

    /**
     * Remove user
     *
     * @param  request  $Request
     * @return  request
     */
    protected function removeGroup($groupId)
    {
        UserManagement::removeGroup($groupId);
        return redirect()->route("ums");
    }

    /**
     * Remove user
     *
     * @param  request  $Request
     * @return  request
     */
    protected function addToGroup(request $Request)
    {
        $userId = $Request->input("userId");
        $groupId = $Request->input("groupId");
        if ($userId && $groupId){
            UserManagement::addToGroup($userId, $groupId);
        }
        return redirect()->route("ums");
    }

    /**
     * Remove user
     *
     * @param  request  $Request
     * @return  request
     */
    protected function removeFromGroup($userId, $groupId)
    {
        if ($userId && $groupId){
            UserManagement::removeFromGroup($userId, $groupId);
        }
        return redirect()->route("ums");
    }
}
