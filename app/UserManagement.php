<?php

namespace App;

class UserManagement
{
    /**
    * Function to add users
    * @param string $userName
    *
    **/
    public static function addUser(string $userName){
        $user = User::create(['name' => $userName]);
        return $user;
    }

    /**
    * Function to return all users
    * @param string $userName
    *
    **/
    public static function getAllUsers(){
        $users = User::all();
        return $users;
    }

    /**
    * Function to remove user
    * @param int $userId
    *
    **/
    public static function removeUser($userId){
        $user = User::find($userId);
        $userGroups = $user->groups;
        foreach ($userGroups as $userGroup){
            $user->groups()->detach($userGroup->id);
        }
        $user->delete();
    }

    /**
    * Function to add users
    * @param string $userName
    *
    **/
    public static function addGroup(string $groupName){
        $group = Group::create(['name' => $groupName]);
        return $group;
    }

    /**
    * Function to return all users
    * @param string $userName
    *
    **/
    public static function getAllGroups(){
        $groups = Group::all();
        return $groups;
    }

    /**
    * Function to remove user
    * @param int $userId
    *
    **/
    public static function removeGroup($groupId){
        $group = Group::find($groupId);
        $usersCount = count($group->users);
        if ($usersCount == 0)
            $group->delete();
    }

    /**
    * Function to add user to group
    * @param int $userId
    *
    **/
    public static function addToGroup($userId, $groupId){
        $user = User::find($userId);
        $groupExists = Group::find($groupId);
        $userIsInGroup = $user->groups->contains($groupId);
        if ($groupExists && !$userIsInGroup)
            $user->groups()->attach($groupId);
    }


    /**
    * Function to add user to group
    * @param int $userId
    *
    **/
    public static function removeFromGroup($userId, $groupId){
        $user = User::find($userId);
        $user->groups()->detach($groupId);
    }
}
