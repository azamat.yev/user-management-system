<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['admin'])->group(function () {
    Route::post('/user/add', ['as' => 'user.add', 'uses' => 'UserManagementController@addUser']);
    Route::get('/user/remove/{userId}', ['as' => 'user.remove', 'uses' => 'UserManagementController@removeUser']);
    Route::post('/user/group/add}', ['as' => 'user.group.add', 'uses' => 'UserManagementController@addToGroup']);

    Route::post('/group/add', ['as' => 'group.add', 'uses' => 'UserManagementController@addGroup']);
    Route::get('/group/remove/{groupId}', ['as' => 'group.remove', 'uses' => 'UserManagementController@removeGroup']);
    Route::get('/group/user/remove/{userId}, {groupId}', ['as' => 'group.user.remove', 'uses' => 'UserManagementController@removeFromGroup']);

    Route::get('/ums', ['as' => 'ums', 'uses' => 'UserManagementController@dashboard']);
});
