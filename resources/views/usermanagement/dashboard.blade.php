<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>User Management System</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: blue;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    User Management System
                </div>
                <form action="{{ route('user.add') }}" method="post">
                    {{ csrf_field() }}
                    <input type="text" name="name" placeholder="user name">
                    <input type="submit"  value="Add user">
                </form>
                <form action="{{ route('group.add') }}" method="post">
                    {{ csrf_field() }}
                    <input type="text" name="name" placeholder="group name">
                    <input type="submit" value="Add group">
                </form>
                <table>
                    <tr>
                        <td>
                            Users
                        </td>
                    </tr>
                    <tr>
                      <th>Id</th>
                      <th>Name</th>
                      <th>Add to group</th>
                    </tr>
                    @foreach ($users as $user)
                        <tr>
                            <td>{{$user->id}}</td>
                            <td>{{$user->name}}</td>
                            <td><a href="{{ route('user.remove', ['userId'=>$user->id])}}">Remove user</a></td>
                            <td>
                                <form action="{{ route('user.group.add')}}" method="post">
                                    {{ csrf_field() }}
                                    <input type="number" name="userId" value="{{$user->id}}" hidden>
                                    <input type="number" name="groupId" placeholder="group id">
                                    <input type="submit" value="Add to group">
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </table>
                <table>
                    <tr>
                        <td>
                            Groups
                        </td>
                    </tr>
                    <tr>
                      <th>Id</th>
                      <th>Name</th>
                      <th>Users</th>
                    </tr>
                    @foreach ($groups as $group)
                        <tr>
                            <td>{{$group->id}}</td>
                            <td>{{$group->name}}</td>
                            <td><a href="{{ route('group.remove', ['groupId'=>$group->id])}}">Remove group</a></td>
                            <td>
                                @foreach ($group->users as $user)
                                    <span>{{$user->id}}</span>
                                    <span>{{$user->name}}</span>
                                    <span><a href="{{ route('group.user.remove', ['userId' => $user->id, 'groupId'=>$group->id])}}">Remove user from group</a></span>
                                @endforeach
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </body>
</html>
